# Helm Argocd

# Gitlab Demo Repo
```bash
https://gitlab.com/quickbooks2018/helm-argocd.git
https://gitlab.com/quickbooks2018/foo.git
https://gitlab.com/quickbooks2018/argocd-image-tag.git
```
### Jenkins Setup
```bash
helm repo add jenkins https://charts.jenkins.io
helm repo update
helm repo ls
helm search repo jenkins
helm search repo jenkins/jenkins --versions
helm show values jenkins/jenkins --version 4.3.20
mkdir -p jenkins/values
helm show values jenkins/jenkins --version 4.3.20 > jenkins/values/jenkins.yaml
helm upgrade --install jenkins --namespace jenkins --create-namespace jenkins/jenkins --version 4.3.20 -f jenkins/values/jenkins.yaml --wait


# Set up port forwarding to the Jenkins UI from Cloud Shell
kubectl -n jenkins port-forward svc/jenkins --address 0.0.0.0 8090:8080

# Jenkins Secrets

kubectl get secrets -n jenkins 

kubectl get secrets/jenkins -n jenkins -o yaml

jenkins-admin-password: b1NuY1NpWmdnR25ZemtuNWx5Mnl0NQ==
jenkins-admin-user: YWRtaW4=

echo -n 'b1NuY1NpWmdnR25ZemtuNWx5Mnl0NQ==' | base64 -d
oSncSiZggGnYzkn5ly2yt5
```

### Argocd Setup

- ArgoCD HA & Non-HA Setup
- https://github.com/argoproj/argo-cd/releases

### Non-HA
```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.7.4/manifests/install.yaml
```

### HA
```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.7.4/manifests/ha/install.yaml
```

- Argocd kubectl commmands
```bash
kubectl get secrets -n argocd
kubectl get secrets/argocd-initial-admin-secret -n argocd -o yaml
echo -n 'UUE1alRPcEZ4RlQ5T0dOWQ==' | base64 --decode
kubectl -n argocd get svc
kubectl -n argocd port-forward svc/argocd-server --address 0.0.0.0 8091:443
```
- Helm argocd chart

```bash
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
helm repo ls
helm search repo argocd
helm show values argo/argo-cd --version 3.35.4
mkdir -p argocd/values
helm show values argo/argo-cd --version 3.35.4 > argocd/values/argocd.yaml
helm install argocd -n argocd --create-namespace argo/argo-cd --version 3.35.4 -f argocd/values/argocd.yaml
```
- Helm Image Updater
- https://argocd-image-updater.readthedocs.io/en/stable/
- https://argocd-image-updater.readthedocs.io/en/stable/basics/update-strategies/
- https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/
- https://argo-cd.readthedocs.io/en/release-2.1/operator-manual/declarative-setup/
### Image pull secrets must exist in the same Kubernetes cluster where Argo CD Image Updater is running in (or has access to). It is currently not possible to fetch those secrets from other clusters
- Note: first create secrets than apply application
- helm Image updater custom "argocd/values/argocd-image-updater.yaml"
- Step-1 Create Access token for gitlab registry
- https://argocd-image-updater.readthedocs.io/en/stable/basics/authentication/
- pullsecret:<namespace>/<secret_name>
- secret:<namespace>/<secret_name>#<field_name>

- https://github.com/argoproj-labs/argocd-image-updater/issues?q=gitlab
- https://github.com/argoproj-labs/argocd-image-updater/issues/483


- Argocd Image Updater Credentials in namespace argocd to read application Gitlab private registry
```bash
kubectl --namespace argocd create secret generic foo-gitlab-credentials \
    --from-literal=username=foo-gitlab-credentials \
    --from-literal=password=glpat-aMagVXPC2L7kLAedCzs_ 
```

- Argocd Image Updater Credentials in namespace argocd to write to helm gitlab repo
```bash
kubectl --namespace argocd create secret generic helm-repo-creds \
    --from-literal=username=ARGOCD-IMAGE-UPDATER \
    --from-literal=password=glpat-CtFg9ziQAVtWk1YDYmNe
```

- Image Pull Credentials in namespace foo-staging
```bash
kubectl --namespace foo-staging create secret docker-registry foo-gitlab-credentials \
  --docker-server=registry.gitlab.com \
  --docker-username=foo-gitlab-credentials \
  --docker-password=glpat-aMagVXPC2L7kLAedCzs_
```

- https://github.com/argoproj-labs/argocd-image-updater/issues/186

- https://argocd-image-updater.readthedocs.io/en/stable/basics/update-strategies/

- Gitlab Deploy Keys to access a specific repo over ssh
- https://docs.gitlab.com/ee/user/project/deploy_keys/index.html
```bash
ssh-keygen -t ed25519 -C "info@cloudgeek.ca" -f ~/.ssh/id_ed25519_helm
```

- Private ssh key for gitlab repo
```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: foo
  namespace: argocd
  labels:
    argocd.argoproj.io/secret-type: repository
type: Opaque
stringData:
  url: "ssh://git@gitlab.com/quickbooks2018/foo.git"
  sshPrivateKey: |
    -----BEGIN OPENSSH PRIVATE KEY-----
    your private ssh key
    -----END OPENSSH PRIVATE KEY-----
  insecure: "false"
  enableLfs: "false"
```  

```bash
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
helm repo ls
helm search repo argocd
helm show values argo/argocd-image-updater --version 0.9.1
mkdir -p argocd/values
helm show values argo/argocd-image-updater --version 0.9.1 > argocd/values/argocd-image-updater.yaml
helm upgrade --install argocd-image-updater -n argocd argo/argocd-image-updater --version 0.9.1 -f argocd/values/argocd-image-updater.yaml --wait
```

- Repo Pull & Push Setup in ArgoCD with Access Toekn

```bash
https://gitlab.com/quickbooks2018/kaniko.git
jenkins
glpat-xVz4Xp-14M6dvMMdWYKx
```

- Jenkins Service Account Role Binding with application namespace default service account
```bash
kubectl create rolebinding jenkins-admin-binding --clusterrole=admin --serviceaccount=jenkins:default --namespace=hello
```

- kind Metrics Server
- https://gist.github.com/sanketsudake/a089e691286bf2189bfedf295222bd43
```bash
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.5.0/components.yaml

cat <<EOF > metric-server-patch.yaml
spec:
  template:
    spec:
      containers:
      - args:
        - --cert-dir=/tmp
        - --secure-port=443
        - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
        - --kubelet-use-node-status-port
        - --metric-resolution=15s
        - --kubelet-insecure-tls
        name: metrics-server
EOF

kubectl patch deployment metrics-server -n kube-system --patch "$(cat metric-server-patch.yaml)"


helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
helm repo update
helm upgrade --install --set args={--kubelet-insecure-tls} metrics-server metrics-server/metrics-server --namespace kube-system
```

- ArgoCD
```bash
https://github.com/argoproj/argo-helm/tree/main/charts/argo-cd

https://github.com/argoproj/argo-cd/releases
```

- application.yaml
```bash
https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/application.yaml

https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/
```


- Bitnami Sealed Secrets

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm search repo bitnami/sealed-secrets
helm show values bitnami/sealed-secrets --version 1.2.11
helm show values bitnami/sealed-secrets --version 1.2.11 > values/sealed-secrets.yaml
helm upgrade --install sealed-secrets --namespace sealed-secrets --create-namespace bitnami/sealed-secrets --version 1.2.11 --wait
```

- install kube-seal for linux

-  https://github.com/bitnami-labs/sealed-secrets/releases

```bash
curl -LO -# https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.20.2/kubeseal-0.20.2-linux-amd64.tar.gz
tar -xzvf kubeseal-0.20.2-linux-amd64.tar.gz
sudo install -m 755 kubeseal /usr/local/bin/kubeseal
```

- Encrypt a secret from a file
- https://github.com/bitnami-labs/sealed-secrets/issues/758

```bash
 --controller-name string           Name of sealed-secrets controller. (default "sealed-secrets-controller")
      --controller-namespace string      Namespace of sealed-secrets controller. (default "kube-system")
```



```bash
kubeseal --controller-name sealed-secrets --controller-namespace=sealed-secrets -o yaml -n sealed-secrets < argocd-repo-secrets.yaml > sealed-argocd-repo-secrets.yaml
```

- ArgoCD CLI Installation

- https://github.com/argoproj/argo-cd/releases

```bash
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
chmod +x /usr/local/bin/argocd
argocd version
```
- ARM
```
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-arm64
chmod +x /usr/local/bin/argocd
argocd version
```


- Argocd CLI Specific Version
```bash
export VERSION=<TAG>
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$VERSION/argocd-linux-amd64
chmod +x /usr/local/bin/argocd
argocd version
```

- Argocd Login (Not do port forward )
- Argocd Port Forward
```bash
kubectl -n argocd port-forward svc/argocd-server --address 0.0.0.0 8091:443
```
```bash
argocd login localhost:8091 --insecure
```
- Argocd Add aws Eks
```bash
kubectl config get-contexts

CURRENT   NAME                                                            CLUSTER                                                         AUTHINFO
                                          NAMESPACE
          arn:aws:eks:us-east-1:831872632567:cluster/cloudgeeks-eks-dev   arn:aws:eks:us-east-1:831872632567:cluster/cloudgeeks-eks-dev   arn:aws:eks:us-east-1:
831872632567:cluster/cloudgeeks-eks-dev
*         kind-cloudgeeks                                                 kind-cloudgeeks                                                 kind-cloudgeeks

➜ argocd cluster add arn:aws:eks:us-east-1:831872632567:cluster/cloudgeeks-eks-dev --name cloudgeeks-eks-dev --kubeconfig ./config
```

- kubectl get context commands
```bash
kubectl config get-contexts

kubectl config get-clusters

kubectl config use-context kind-cloudgeeks

kubectl config use-context arn:aws:eks:us-east-1:831872632567:cluster/cloudgeeks-eks-dev
```

- curl pod to test the network connectivity from a namespace

```bash
apiVersion: v1
kind: Pod
metadata:
  name: curl-pod
  namespace: argocd
spec:
  containers:
    - name: curl
      image: curlimages/curl
      command: ["sleep", "infinity"]
```      

- Argocd Add new cluster in kind kubernetes

```config
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUMvakNDQWVhZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJek1EWXlNREExTlRZME5Wb1hEVE16TURZeE56QTFOVFkwTlZvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBT21iCkhOWkNNdjRDQ0JhZFpvZUdHRkx2WGtPZEhZSE8rM3FPRXU0VzRaTlEwVVBWbTF3RXJJdDZpaHkzaFdCRmppVm4KT29JclYyVFM4NHcrS2t3S2pQUEtOQnU1eWJRWU1LaGppR3hnNzByQTE3dElrNmJVelZZYis0MEFUdWhmTDVWTgpvZGF5aU4rNndnUkhPZjNQVkNwdXc0L0FLZVRPczZqY2JsYzlLdiszdFRqUFdkUTI0VTJRWHRjRGh4dGRPcU8yCmljV2U5bHpLaUdjWmJZNzMxYVVMZFpGYnZENUoxOFplTWg0M29iYm1zaHVIZFlZSDJVcC9vLzNaeFhLS0hzRFMKK2ltTGJ6Y3o1Y3B3MnVFSEJEWmFPRzJCSUovZStzTGdlbm5mWmEzOWVKa1c5NG9iSU1LQkorN1NkcE44Qm8wdQpBeDQ3MXRJYkJlL2JYK2tEUHAwQ0F3RUFBYU5aTUZjd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0hRWURWUjBPQkJZRUZMWnVwblNZR2MwRXR6NW5Sd00zUEp2VEJTekpNQlVHQTFVZEVRUU8KTUF5Q0NtdDFZbVZ5Ym1WMFpYTXdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBR0tHR01NZ2l6UXFvbGRsZ0k5UQp2WGFmNFl1ZGNTRkVITkEzOEoxOUhxcDhubmVVSE8xd05yQjQwdi9VTXNJRE5tMVhWSmsrL3Jha3YvQytuQy8rClUvQkpoQldQNzIrbEQrSnprcm5BZTV3MC9YWTNzOThPNFczc2pHazliNWNub1dlYVJVWGdSMURvaDlscDdZdHMKSUF6U01sZlo2OTV3cmVaMllldGlVNGhxK0Z1eTRja1BqYnNiODRidFMvUG9YR0ovRW4yNU9FVm1kcDk5K1BvcgpSRDFLUDliVHFTcVNMaUhxWUpXemIrOGhJUHpjZnhhMGV1ekdMTlA5cmN1bCtRd2pJVWd5QTcxZWpiRUZpSm10CktaRTloWHRhQ3RlV2JKdEZnR3kvclF2V0VuVmJteHhUcXR0QnVLVDdtUzZsRDJZVkpjWktmd0U2bnZHOHBqYkEKVlZjPQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    server: https://172.18.0.2:6443 #-----------------> this is internal ip of docker container using port 6443
  name: kind-cloudgeeks-local-1
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUMvakNDQWVhZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJek1EWXlNREExTlRjeU1Gb1hEVE16TURZeE56QTFOVGN5TUZvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBS1dVCkxFWGQ5WFYveUZGZDNISTZpcm5nSnVJT3hYWm1EdjdyYlFLVUxOZVBabFpHL2ZTZmk1WmxteEVHbjgzTjFFQm4KcVlNbG9BTWtPUmhxdVp6Vms1dDIwcVZXRUFVbUpYeXZkWEZHemdrczMzSGdVNU9qdmhZR0VhS1JJOGprOGJoKwoxMkNMUjFIR0s4T2tWbzZ6QUZnQ1BOUDhFbkxkQ1hRZGVTdG56TE5mV0dSa3pSdVBvTW9xazh3Z20yOG1uYzB1CjNxTlhOZTdPQTVkT3BwOEdQZ05Zd2lpQXhKcjBMLzlrVUlRcFhXaVZYVWJQNUFCOGtvanJXYVc0dHB4TFNBb2YKWTd2TUxxUkhMYWozV1dFNU8vbzhtSENwVVNEUVh4L1kwZllvd3J1NlMxcGpiSWFDV0tRd1RwSGR1Q3BrVXh6ZApBLytoeTYydWlrV01lWmkvVUdzQ0F3RUFBYU5aTUZjd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0hRWURWUjBPQkJZRUZCaXQ1aHlVYmpqTVNRUlZQRDdZVGI2Szdid3NNQlVHQTFVZEVRUU8KTUF5Q0NtdDFZbVZ5Ym1WMFpYTXdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBSlZMTEdSNkZzbHhiNVhQd3lPMApBWjBnRzNXK1RCTDVQbk5OUTFJR1J6SXU2YldmS2dqWHR2Q1pLcEYzbmFPWllNY0dpRThidUtBQ2kzbFFzT1VrCjZ0cjZaVUtLMjJpc3E5RFlaWVFpZ2dFa2Z4eDJ3OFBNQm43K29CNDhMZGJJNnY3WWErdU5oc0FncXZ6RGFENUoKZzVnb3RlTmZDOG1yKzVuM0ZJdUlYRTl1dm1ndkhKYW83bHVkL1J2bUo0U2FzWUxzdzRwKzVVelFVNzdKWmRyago5dGlDTEtSK0JReFgrS0pJQUp6enNwU1dvUHcwYnZHb2FxTnh4aDI2Rk1SZzVNOXFkWTFoRFdSanFKQ0ptUlpYCkpXNUtnd05WVjMrenZ3ZWVtczIwYXFBM0xiMytxSmJmNUE0OG90djIzbGV0Q3dsM1hrdldLb2dUSGkrNnh3RkcKUE1NPQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    server: https://172.31.10.247:8444
  name: kind-cloudgeeks-local-2
contexts:
- context:
    cluster: kind-cloudgeeks-local-1
    user: kind-cloudgeeks-local-1
  name: kind-cloudgeeks-local-1
- context:
    cluster: kind-cloudgeeks-local-2
    user: kind-cloudgeeks-local-2
  name: kind-cloudgeeks-local-2
current-context: kind-cloudgeeks-local-2
kind: Config
preferences: {}
users:
- name: kind-cloudgeeks-local-1
  user:
    client-certificate-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURJVENDQWdtZ0F3SUJBZ0lJSWZmR01Kd3NTMmd3RFFZSktvWklodmNOQVFFTEJRQXdGVEVUTUJFR0ExVUUKQXhNS2EzVmlaWEp1WlhSbGN6QWVGdzB5TXpBMk1qQXdOVFUyTkRWYUZ3MHlOREEyTVRrd05UVTJORGhhTURReApGekFWQmdOVkJBb1REbk41YzNSbGJUcHRZWE4wWlhKek1Sa3dGd1lEVlFRREV4QnJkV0psY201bGRHVnpMV0ZrCmJXbHVNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQW8raGwvZkQ1Y2V0RDNkd1EKWmxrNWpQV216OUNWQVBjcVpvbGF5K2FIUEdjOVh6Qk1kdnZsamZ5UFoxbllCRm5BajRmSFRwQlNUUkhyVU4zaApXN2RSOTBGSmJCRlM1OHNkcTdza3VtWkMvRWJZdi9NK3JHNU5XQ2FIdDZvUHI2YmJGWkQ4dktJaC94WGoyank0CkFjRGM4OGhSSXViQkxVVnRPa2lkaVdjTWZQQVBoOFU2OVpCUWVGUGRwU3pUem0rNStSQVNZbnB5eWxNTEFqMG8KMjRyOTVmWVBBTlNBMEVsQmhkQnhZdUJpZTJkQmI1aW9QUDJyZlRpWVpucVdYUjl3ZjkyUG9nZ29jNnQ2OE0rUgovZ1ZCR3VLaGtuNW5ZN2xtK29jUm53YkxZbjV4TjU3MG90WmlCSzA2eURVejhsWnQ3VEpSd1ZMSG45cllCUm1SCjlTQ0tOd0lEQVFBQm8xWXdWREFPQmdOVkhROEJBZjhFQkFNQ0JhQXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUgKQXdJd0RBWURWUjBUQVFIL0JBSXdBREFmQmdOVkhTTUVHREFXZ0JTMmJxWjBtQm5OQkxjK1owY0ROenliMHdVcwp5VEFOQmdrcWhraUc5dzBCQVFzRkFBT0NBUUVBcktlTnVURUVlNFNuNnVvaDlCWVh1eGsxWWJpVE8wZnFUVUpvCkMwbEFWVmNLaWZQSFVWL0FOd3h4N2tYc0JtL2xNUDloMzZQMHJvQVNMNzlUZFdwK0tBTi9iWFhzRzk4QURsTHYKcTFXR2JtYmxRK2NmTUNTODRHVW92ZDBlN3pSdzY5TWZYRXFJY1c5U3EzemYremM1V0lpaW1xUU4xWU1VRnUxKwp6c2xYVXlHcU03TjJOMHlHZzVKU25kd0FvZ09FVVlSNXlhNU5WNmFyQWZHTSt6SjQ3aUQ3aGE4Q2l4bzFuSmJGCmMyWWV6eVA0M1R5ZnVFVmJxY0p2a1YzaEZ3amVmVHkxNU5aVk5IWWxEV2RFMk96MnczeUVRK2sraUtVTVNvWW4KNDRDamR0dk9uRldxdU4wSDIyOC9pVllmY295QTM3TmRlVFdaVzFhL2xNcVUxYUNHZ3c9PQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    client-key-data: LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFcEFJQkFBS0NBUUVBbytobC9mRDVjZXREM2R3UVpsazVqUFdtejlDVkFQY3Fab2xheSthSFBHYzlYekJNCmR2dmxqZnlQWjFuWUJGbkFqNGZIVHBCU1RSSHJVTjNoVzdkUjkwRkpiQkZTNThzZHE3c2t1bVpDL0ViWXYvTSsKckc1TldDYUh0Nm9QcjZiYkZaRDh2S0loL3hYajJqeTRBY0RjODhoUkl1YkJMVVZ0T2tpZGlXY01mUEFQaDhVNgo5WkJRZUZQZHBTelR6bSs1K1JBU1lucHl5bE1MQWowbzI0cjk1ZllQQU5TQTBFbEJoZEJ4WXVCaWUyZEJiNWlvClBQMnJmVGlZWm5xV1hSOXdmOTJQb2dnb2M2dDY4TStSL2dWQkd1S2hrbjVuWTdsbStvY1Jud2JMWW41eE41NzAKb3RaaUJLMDZ5RFV6OGxadDdUSlJ3VkxIbjlyWUJSbVI5U0NLTndJREFRQUJBb0lCQUc4NkVLUS90M2FIMlZrZgpzZjFTUlF5VDBxd3Bka1BlZG1aNGhTWGkvUmlQVW9WYTZEakIvV2ZaNEpLWTFFODNmTVdwdFNWcUtIOW1UcEprCnp6NEc2OWRtOVdVaGFHb2xXckFqNGV1ZjQ0c0FwS3UxYkcxa3VKVytKOUVKMVhVMFBHMnBJdWo2K1NqNStOSXEKN0E4OGluUGdGcmNoY2l1L2RqemI5Qk8rMnltWVErK0oxTm90bW1QanNsNFAzVUx5elBLNTlWb1pjUWpvalNQawpsQjJ2bG12VVlqdHU2NWdHMlhSY3lsYSsrVFNkUFB5aUllcW1kT3RwYkVrUDZBQ0xoZWxaNGpQYSs3dUdld3FICmoxbGZLUUh5cVNDRDVMeUZaYmdGU1pWMHU4clgvdVY1MTJxWjZha2p4VnhqSTNTRlpPaTV6aE1lcHFYVklMdzAKUFVweUlWRUNnWUVBd2J2dlE1Z1lnMUFhWlVjR0FsbWh5c2s4bEhRQXpjUjBTQVg1bmpXVTlTQVJxai9Xa2hQMgpKZkZmVFQzbTlmdVMvdWprZTl1RE00djRCRjJ4WnB6YWR5WWN1cVZzVURDcU9ySkJXWVRlU0FjSXRpRlJXV2FDCm8vYldkNlhJdWZkdjA1YnFLbktSM2VyRWVBUVF4VHdta0ZXN2NZcTVCZkg2VmdCUVFBNDFYNDhDZ1lFQTJKWnAKdEl2QWRFUDJyeUQ5Vmg5eG9mT1V5emlZTVVwY2ErUFhha2pxL01CZmZHaFdsMGxGV085ZmNyS1ZVbS9xaWY5RwpUTGNWZm5RYVA3Q3d1a1liTGo3NGFXRkVwUnBCT3A3U0NzTktYRXJqbjlmMFkxNjFKc2sySjlzOUlOcHYzclN1Cm5Ta0RSMEc5aHpGOGMvYWZ0cnhsLzkrVmpNK0FOcFNZb3ROdjF0a0NnWUJ3QmtRc1NkNml6OGJ6bVVZWVJjd1EKZnB6Ukd1TDhHUVZFUUhBKzA2ejJzVHlGdmJSTmM4MTUwMERRWVdNVUNnek42YzEwOFZIRWxNTXRneC81dmVsMQp6RXdBZ1hkSVIxdW41VHQwa0NnWWJqalRXQ0M2R1ZTaGl6SW9yV3lNWEZBRmdkYVdCUG5qNE1VS3dROFJmZDZ1CngyUkVHM3N4dk9LTXFGRy9PSGdVY1FLQmdRRFM2M2U1V1Rkc2doc2paVThRcGNKMVlzNk9NV2tVNTBPbDFkWWMKTFRLYVBZVS9sQ1J2WSsxaXRJb1JHcFNNR1cwZ1FqYmwwbzNkL1picVcxbGZyWkllc1RnYzk1cHY1bWVMZk1zegpyQWNvWmMzU0JlNTdMdW5mQWFmNW5HbWxLdFF5MzJZRXFzSUVqS0xUbTMrRk5yRkd0YVhuVG9kRy8zUHVzenNKCnBqcU4wUUtCZ1FDNXZTckhkTDlFdFVCa3FWU2czUmdHNGk1N0NvRVN6dFZiRUl1bWxWTjVBdFY1NmhCditVOEwKYUdCSkRLSTY3U1BTalhTWWJZREZjQ3U4YjNHTkk5UmExZ3pxd2hDM1luZSs4Mzd3TXFqTTVBdG5NTzBCMEtwOAptR2k2UDZnR0VwNzlGN0tkVUZQWmJDZ2ZOaTJYdjJCdkV4ZUhOc0x5NHhKTzVzWGt3UmRFZ2c9PQotLS0tLUVORCBSU0EgUFJJVkFURSBLRVktLS0tLQo=
- name: kind-cloudgeeks-local-2
  user:
    client-certificate-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURJVENDQWdtZ0F3SUJBZ0lJWC9pczR5aVNpYXN3RFFZSktvWklodmNOQVFFTEJRQXdGVEVUTUJFR0ExVUUKQXhNS2EzVmlaWEp1WlhSbGN6QWVGdzB5TXpBMk1qQXdOVFUzTWpCYUZ3MHlOREEyTVRrd05UVTNNalJhTURReApGekFWQmdOVkJBb1REbk41YzNSbGJUcHRZWE4wWlhKek1Sa3dGd1lEVlFRREV4QnJkV0psY201bGRHVnpMV0ZrCmJXbHVNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQXo5WmFFblpRSGhwSGZyZ2UKNUFlUlU3aVRwTUt3Y0U0UjIwVUtuTTRQTUY5cExaaHcrSTVtdXpPVlRoUDQvdW56dHFPeVBqS2pCcHI0Mlh5OQpVWkpKbEZNcjRURWcyejVQbnJ0bjByRm5ySjZPeG0xVzVNZ29Gb0kyYnZhSkVXVDQ5RkoxTzh2c3huNGdZMEVtCmJyNGJRR3hzNzZydGxodlQvSU1ERDd3MVNwWkVXK0hUd1kvUUY5dTErZ3RSSWt1bmZsT0ZjeEJpem1tR0E5aTMKSVFBYVpTS0RVK2R2UXZhZzRnMnQ4MU5TSlVkVlpmb1VabkRNaFJORWIxeGNGN2dMZ25kQVNoQmhnSi9qMHhubApsc3dKLzBCUzY0RkRXemJ5d1U4dkY3RGhHRmVySCtyOE42K2t6YWFCbzQrdWROREtkbnI0Tk1xM0ZMaVF4aVJOCkkwRWlXUUlEQVFBQm8xWXdWREFPQmdOVkhROEJBZjhFQkFNQ0JhQXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUgKQXdJd0RBWURWUjBUQVFIL0JBSXdBREFmQmdOVkhTTUVHREFXZ0JRWXJlWWNsRzQ0ekVrRVZUdysyRTIraXUyOApMREFOQmdrcWhraUc5dzBCQVFzRkFBT0NBUUVBYllaZURtWXc3cEt6WW9kQ0NDZnJjdExDRVdHTy9nTm5CT0srCit1Y044RHNxVUtKKytyd1ozeDE3cVNVSlZoU3IwVzlQSE1iQkhzaW9TU0F2dXhRT01pQ0kxWW82b3dEVFJrWk8KVFhKRFlDaHE1Z3AyaEh5TUhjREJ1aTZIK2E2Q3FTY01kRkNOVU5KekdnSjk2Wk1MZFRqZmg3RDh1VXdSeVdieApnUlNjc3UwZGZoZjhOeURjMmY2Vi9Ga3NhenRtb1RxMi8xeUJVOXBURlVMSFkyZS9RSThTdEdLaE5nUDNNMHZRCklpd2RXS0JuMG44V0g0VDAvakVWeHlSdDVBLzQwR0t1TGk4TWZ4RW5XeFROaWdkYmdkeGxFa3hLMEFvUXFaV0IKZm41TW5IbmxHZWl2UnJJdE8zakdjeFNYOVhwKzRGUHZycnRFVUhEYWtEUldDbnUrbFE9PQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    client-key-data: LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFcEFJQkFBS0NBUUVBejlaYUVuWlFIaHBIZnJnZTVBZVJVN2lUcE1Ld2NFNFIyMFVLbk00UE1GOXBMWmh3CitJNW11ek9WVGhQNC91bnp0cU95UGpLakJwcjQyWHk5VVpKSmxGTXI0VEVnMno1UG5ydG4wckZucko2T3htMVcKNU1nb0ZvSTJidmFKRVdUNDlGSjFPOHZzeG40Z1kwRW1icjRiUUd4czc2cnRsaHZUL0lNREQ3dzFTcFpFVytIVAp3WS9RRjl1MStndFJJa3VuZmxPRmN4Qml6bW1HQTlpM0lRQWFaU0tEVStkdlF2YWc0ZzJ0ODFOU0pVZFZaZm9VClpuRE1oUk5FYjF4Y0Y3Z0xnbmRBU2hCaGdKL2oweG5sbHN3Si8wQlM2NEZEV3pieXdVOHZGN0RoR0ZlckgrcjgKTjYra3phYUJvNCt1ZE5ES2RucjROTXEzRkxpUXhpUk5JMEVpV1FJREFRQUJBb0lCQUNwNUlxVWRPcGc4UkpvWAo1SEF6UDdhOVJLYnFCb2FjM2JkU3I2MkZ5c0xXN0xhZHJaQ09BVjZHVngvQ295THRDcG9yYjlTWUlvcktCc3JqClhBa2NmUjdydEU3cjdFd0J1dUcwYUs0OUJQUFkzMUticTZOTGFDM1paR2hleDNvZmlXVGNxUXFVVFFudllvMEcKRm1XUlFoNXpTZnZja1hzWXFzU3l2V0JzMVBNdHpmZ2NXUnFCb0h0K1I5T0VJZzNoN0EzMzcrWlFsb29Dd004aApJZVc2cHV4cVdLRERZNHpnRFR3MmtzMnZuby8xM1NkcmxKalJVTEtLV1krcFVrUjFMaVZPQkFJOGRaNUdxMnlHCk9xalN2TVgvQ1ErWU9JYzFJaTNVWklwbEcySDBhaHJvSndjMzd6THpWUzVyN05NaE1HUmMxb3ZIQ2hCVVNnOGYKUzZQdHlnRUNnWUVBNUZUaW8ycHhKTzhlVkRxSDFYZ1JtcTZSc0k3VXJhOUpiQXR2Q0t3QXVPazR5ckp4VFpsNAoxYTBBU25FT040OXVZS2kxU0xzQ3RVZ09sNG53MFFQc2tCK3ZHcWVoUEtScDAvRWlHQUZ6b2RwYzViZlI4WFF3CmlmWU05QS95eTd5NDVLODVWZ3lNcHBLcUk3SkZCV0gwano5T2RLbWFBOEtORm5oQS9pVzRhcGtDZ1lFQTZRVzEKeVFLamx6MVVvT01XSGtwZVFRakxmS2VFTVQxTk1pY0Qxczl2ekxJZHpxcm5WeWlOaGRyYTFLUjFKeUhvL2FrTQoreVJ5QkwwQi8xWXR0WmVVbktlbDdSYlFLL25RWVB0TklHTGdKWFYxb2NyNGJpOEVQZldNVDZpbm9ORGFaRnVXCkxzK0tlSWJsOWxqTFgxNEVvUnFyZVU1QzRhRFdsVDl1UEt5bkRjRUNnWUVBazNDWTFLczBHb1JwMGp2bFlKdmkKeWo1MWgwd0QyNCszMnBQYmcvOEZqR0hWTVZaZXdad2JvZWJQckVJcXl3Zk1YNUF4VjZDaTFBcU9tWjRTS2ZYZAp1UWNzbkFxb25DOFN3ekFQTXU4bEN0dWxaeFQyOVhKd3NYVjMwZFhHazNidThZU1FncGNoNzZIL1NZT2cvQ1BQCnR2eExIaHd5ZnNFUE4vWTVDYU96M09rQ2dZQmYyOGw4ZDJsMHZmWWllVWlRRFdmWXVSVjhvaXp2VkpoOUdTK3IKSG9xUFduYUluaVdzNVRIUzdzVTIxTENjK0lZd1UyclRwWnFCK0g3cDJ6blNpaGpJbU83WktabFpmZHNMQ2xZZwpESXpJM0JrWUJiZE5IR3BTVXNEMXhCdXhQRG1iTDFJSXo3ZlcvU2Q4ditwcHdQT0drc0lYbE9wT2h5dS9HZWp2CmJOY01BUUtCZ1FET3JFSzJGM05BdmVxaDdkSS9xME52bGhWaHVjTGRWNlluNjlLbXNCRGNOemljalJ6THc4RzIKbTlOMW55STh5M0Z6WGpUVFc2MUtISERyWm9IYm5KNXJQcnd5clh5M2RrM05tOUR5Q2hrWUY4ZVhUQVlLb2lETwpJU1NRdjg3UDNjdnNXM0JXNlpVSi9kR2s4MmszTmwyM1lEYjZKRVFlNUNhR1o5aFpHTWQyS3c9PQotLS0tLUVORCBSU0EgUFJJVkFURSBLRVktLS0tLQo=
```    


### Helm ArgoCD Repo with Gitlab

- Helm cheat sheet https://github.com/quickbooks2018/aws/blob/master/helm-useful-commands

- https://docs.gitlab.com/ee/user/packages/helm_repository/

- My Gitlab Repo ---> https://gitlab.com/quickbooks2018/helm-argocd.git

```helm
#token
argocd
123456789

projectid: 46536567

helm repo add --username <username> --password <access_token> project-1 https://gitlab.example.com/api/v4/projects/<project_id>/packages/helm/<channel>
helm cm-push mychart-0.1.0.tgz project-1

helm repo add --username argocd --password 123456789 devops https://gitlab.com/api/v4/projects/46536567/packages/helm/stable


# https://github.com/chartmuseum/helm-push/#readme
# helm plugin install https://github.com/chartmuseum/helm-push
Downloading and installing helm-push v0.10.1 ...
https://github.com/chartmuseum/helm-push/releases/download/v0.10.1/helm-push_0.10.1_darwin_amd64.tar.gz
Installed plugin: cm-push



helm cm-push blue-0.1.0.tgz devops
```

- ArgoCD Helm URL
```helm
https://gitlab.com/api/v4/projects/46536567/packages/helm/stable
```

- Helm remote repo
```helm
helm repo add myrepo https://gitlab.com/api/v4/projects/46536567/packages/helm/stable
helm repo ls
helm search repo myrepo
helm pull myrepo/blue
helm install blue blue-0.1.1.tgz
```

### k8 Trouble Shooting add a new pod in same namespace example curl pod as mentioned above in example do network testing
```bash
curl -v telnet://hostnameorip:port
```

- k8 Trouble Shooting add a new container in same pod
```bash
kubectl edit deployment my-deployment
```
```yaml
spec:
  template:
    spec:
      containers:
      - name: my-container
        image: my-image
```
- To add another container, you add another item to the containers list:
```yaml
spec:
  template:
    spec:
      containers:
      - name: my-container
        image: my-image
      - name: my-second-container
        image: my-second-image
```
- Status of deployment, you will see new container is added
```bash
kubectl rollout status deployment my-deployment
```